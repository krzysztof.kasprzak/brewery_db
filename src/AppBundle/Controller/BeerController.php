<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class BeerController
 * @package AppBundle\Controller
 */
class BeerController extends Controller
{
    /**
     * @Route("beers", name="beers")
     * @Route("", name="homepage")
     * @Template("@App/beer/index.html.twig")
     */
    public function indexAction()
    {
        try {
            $randomBeer = $this->get('app.service.brewery_db_service')
                ->random();
        } catch (\Exception $exception) {
            $this->logException($exception);
            throw new NotFoundHttpException();
        }

        return [
            'randomBeer' => $randomBeer
        ];
    }

    /**
     * @param Request $request
     *
     * @Route(
     *     "beers/search",
     *     name="beers_search"
     * )
     * @Method({"POST"})
     * @Template("@App/beer/searchItems.html.twig")
     *
     * @return array
     */
    public function searchAction(Request $request)
    {

        try {
            $searchPhrase = $request->request->get('search');

            $this->get('app.service.validation_service')
                ->validateInputSearchPhrase($searchPhrase);

            $args = [
                'type' => $request->request->get('type'),
                'q' => $searchPhrase
            ];
            $beers = $this->get('app.service.brewery_db_service')
                ->search($args);
        } catch (\Exception $exception) {
            $this->logException($exception);
            $beers = [];
        }

        return [
            'beers' => $beers
        ];
    }

    /**
     * @Route("beer/random", name="beer_random")
     * @Method({"GET"})
     *
     * @return JsonResponse
    */
    public function randomAction()
    {
        $response = new JsonResponse();
        try {
            $response->setData(
                $this->get('app.service.brewery_db_service')->random()
            );
        } catch (\Exception $exception) {
            $this->logException($exception);
            $response = ['error' => $this->get('translator')->trans('error.unexpected')];
        }
        return $response;
    }

    /**
     * @param $breweryId
     * @param $page
     *
     * @Route(
     *     "beers/brewery/{breweryId}/{page}",
     *     requirements={"page" = "\d+"},
     *     defaults={"page" = 1},
     *     name="beers_from_brewery"
     * )
     * @Method({"GET"})
     * @Template("@App/beer/searchItems.html.twig")
     *
     * @return array
    */
    public function fromBreweryAction($breweryId, $page)
    {
        $args = [
            'p' => $page
        ];
        try {
            $beers = $this->get('app.service.brewery_db_service')
                ->fromBrewery($breweryId, $args);
        } catch (\Exception $exception) {
            $this->logException($exception);
            $beers = [];
        }

        return ['beers' => $beers];
    }

    /**
     * @param $exception
     */
    private function logException(\Throwable $exception):void
    {
        $this->get('logger')->addError(
            $exception->getMessage() . $exception->getTraceAsString()
        );
    }
}
