<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RefreshRandomBeersCacheCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:refresh_random_beers_cache_command')
            ->setDescription('Invalidates and worm up random beers cache')
            ->addArgument('amount', InputArgument::REQUIRED, 'Beers amount to save');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $amount = $input->getArgument('amount');
        if (empty($amount) || !is_numeric($amount) || $amount<0) {
            throw new \InvalidArgumentException(
                'Amount parameter must be positive integer'
            );
        }

        $breweryService = $this->getContainer()->get('app.service.brewery_db_service');

        $breweryService->getBeersWithDescriptionAndLabel($amount, true);
        $output->writeln('Cache refreshed');
    }
}
