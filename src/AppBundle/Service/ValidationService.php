<?php

namespace AppBundle\Service;

class ValidationService
{
    /**
     * @param $phrase
     */
    public function validateInputSearchPhrase($phrase)
    {
        $pattern = '/^[\w\d -]+$/';
        if (!preg_match($pattern, $phrase)) {
            throw new \InvalidArgumentException(
                "Phrase: '$phrase' not match pattern: '$pattern'"
            );
        }
    }
}
