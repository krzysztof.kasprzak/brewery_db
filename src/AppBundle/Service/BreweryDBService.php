<?php

namespace AppBundle\Service;

use Doctrine\Common\Cache\CacheProvider;

class BreweryDBService
{
    const MAIN_PAGE_RANDOM_BEERS_CACHE_KEY = 'brewerydb.beers.main_page_random';

    /**
     * @var BreweryDBClientService
     */
    private $client;

    /**
     * @var CacheProvider
     */
    private $cacheProvider;

    /**
     * BreweryDBService constructor.
     * @param BreweryDBClientService $client
     * @param CacheProvider $cacheProvider
     */
    public function __construct(
        BreweryDBClientService $client,
        CacheProvider $cacheProvider
    ) {
        $this->client = $client;
        $this->cacheProvider = $cacheProvider;
    }


    /**
     * @return \stdClass
     * @throws \Exception
     */
    public function random():\stdClass
    {
        $beers = $this->getBeersWithDescriptionAndLabel();

        $randomKey = array_rand($beers);
        return $beers[$randomKey];
    }

    /**
     * @param string $breweryId
     * @param array $args
     * @return array
     */
    public function fromBrewery(string $breweryId, array $args = [])
    {
        $endpoint = "/brewery/$breweryId/beers";

        $response = $this->client->request($endpoint, $args);
        $beers = $this->getDataFromStdClass($response);
        $validBeers = $this->getBeersWithDescription($beers);

        return $validBeers;
    }

    /**
     * @param int $expectedMinAmount
     * @param bool $refreshCache
     * @return array
     */
    public function getBeersWithDescriptionAndLabel(
        $expectedMinAmount = 200,
        bool $refreshCache = false
    ):array {
        $cacheKey = self::MAIN_PAGE_RANDOM_BEERS_CACHE_KEY;
        $cacheLifetime = 42000; //60 * 60 *12

        $cacheProvider = $this->cacheProvider;

        if (!$refreshCache && $cacheProvider->contains($cacheKey)) {
            $beersArray = $cacheProvider->fetch($cacheKey);

        } else {

            $styles = $this->styles();

            $beersArray = [];

            while (!empty($styles) && count($beersArray) < $expectedMinAmount) {

                $randomStylesArrayKey = array_rand($styles);
                $randomStyle = $styles[$randomStylesArrayKey];

                unset($styles[$randomStylesArrayKey]);

                $randomStyleId = $randomStyle->id;
                $beersRequirements = [
                    'hasLabels' => 'Y',
                    'withBreweries' => 'Y'
                ];

                $beers = $this->beersByStyle(
                    $randomStyleId,
                    $beersRequirements
                );

                $validBeers = $this->getBeersWithDescription($beers);

                $beersArray = array_merge(
                    $beersArray,
                    $validBeers
                );
            }

            $this->cacheProvider->save($cacheKey, $beersArray, $cacheLifetime);
        }

        return $beersArray;
    }


    /**
     * @param $args
     * @return array
     */
    public function search($args):array
    {
        $endpoint = 'search';
        $response = $this->client->request($endpoint, $args);
        $beers = $this->getDataFromStdClass($response);
        $validBeers = $this->getBeersWithDescription($beers);
        return $validBeers;
    }

    /**
     * @return array
     */
    public function styles():array
    {
        $cacheKey = 'brewerydb.styles.list';
        $cacheLifeTime = 86400; //60*60*24
        $cacheProvider = $this->cacheProvider;

        if ($cacheProvider->contains($cacheKey)) {
            $styles = $cacheProvider->fetch($cacheKey);
        } else {
            $endpoint = 'styles';
            $styles = $this->client->request($endpoint)->data;
            $cacheProvider->save($cacheKey, $styles, $cacheLifeTime);
        }

        return $styles;
    }

    /**
     * @param int $styleId
     * @param array $args
     * @return array
     */
    public function beersByStyle(int $styleId, array $args = []):array
    {
        $endpoint = 'beers';
        $args['styleId'] = $styleId;
        $result = $this->client->request($endpoint, $args);
        return $this->getDataFromStdClass($result);
    }

    /**
     * @param $beers
     * @return array
     */
    private function getBeersWithDescription($beers):array
    {
        $validBeers = [];
        foreach ($beers as $beer) {
            if (isset($beer->description)) {
                $validBeers[$beer->id] = $beer;
            }
        }
        return $validBeers;
    }

    /**
     * @param $response
     * @return array
     */
    private function getDataFromStdClass($response):array
    {
        return isset($response->data) ? $response->data : [];
    }
}
