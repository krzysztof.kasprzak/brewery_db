<?php

namespace AppBundle\Service;

use AppBundle\Exception\InvalidResponseStatusException;
use Doctrine\Common\Cache\CacheProvider;
use GuzzleHttp\ClientInterface;

class BreweryDBClientService
{

    const METHOD_GET = 'GET';

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var string
     */
    private $format = 'json';

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var ClientInterface
     */
    private $guzzleClient;

    /**
     * BreweryDBClientService constructor.
     * @param string $baseUrl
     * @param string $apiKey
     * @param ClientInterface $guzzleClient
     */
    public function __construct(
        string $baseUrl,
        string $apiKey,
        ClientInterface $guzzleClient
    ) {
        $this->apiKey = $apiKey;
        $this->baseUrl = $baseUrl;
        $this->guzzleClient = $guzzleClient;
    }

    /**
     * @param string $endpoint
     * @param array $args
     * @param string $method
     * @return \stdClass
     * @throws \Exception
     */
    public function request(
        string $endpoint,
        array $args = [],
        string $method = self::METHOD_GET
    ):\stdClass {

        $url = $this->buildUrl($endpoint, $args);
        $client = $this->guzzleClient;
        $response = $client->request($method, $url);
        $data = \GuzzleHttp\json_decode(
            $response->getBody()->getContents()
        );

        if (!$data->status === 'success') {
            throw new InvalidResponseStatusException(
                "Response status not valid. Url: $url"
            );
        }

        return $data;
    }

    /**
     * @param string $endpoint
     * @param array $args
     * @return string
     */
    private function buildUrl(string $endpoint, array $args):string
    {
        $args['key'] = $this->apiKey;
        $args['format'] = $this->format;
        $url = $this->baseUrl . "/$endpoint?" . http_build_query($args);
        return $url;
    }
}
