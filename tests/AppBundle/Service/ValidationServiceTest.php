<?php

namespace AppBundle\Service;

class ValidationServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ValidationService $validationService
     */
    private $validationService;

    public function setUp()
    {
        $this->validationService = new ValidationService();
    }

    /**
     * @dataProvider correctPhrasesProvider
     */
    public function testCorrectValidationPhrases($phrase)
    {
        try {
            $this->validationService->validateInputSearchPhrase($phrase);
        } catch (\InvalidArgumentException $exception) {
            $this->fail("Phrase $phrase should not throw exception");
        }
    }

    /**
     * @dataProvider incorrectPhrasesProvider
     * @expectedException \InvalidArgumentException

     */
    public function testIncorrectValidationPhrases($phrase)
    {
        $this->validationService->validateInputSearchPhrase($phrase);
    }

    public function correctPhrasesProvider()
    {
        return [
            [22],
            ['test'],
            ['asdasdasdas'],
            ['asd asd as das'],
            ['32d3d asd33 32f'],
            ['- asd -d32 3 32 dd']
        ];
    }

    public function incorrectPhrasesProvider()
    {
        return [
            [''],
            ['tes.t'],
            ['asdasda$sdas'],
            ['asd@ asd as das'],
            ['32d3d asd3[3 32f'],
            ['- asd -d3"2 3 32 dd']
        ];
    }
}
